import React from 'react';
import GoogleLogo from '../assets/img/google_logo.png';


function SignInButton() {
    return (
        <div className='btn-sign-in'>
            <img src={GoogleLogo} alt='Google Logo' width="40" />
            <span className='sign-in-text'>Sign in with Google</span>
        </div>
    );
}
export default SignInButton;