import { Fragment } from "react";
import Button from "@mui/material/Button";
import { Toaster, toast } from "react-hot-toast";
import { BASE_URL, API_VERSION } from "../shared/config";
import { TextField } from "@mui/material";

const AvailableDesks = ({
  book,
  startDate,
  endDate,
  isAvailable,
  daysOfWeek,
  daysOfMonth,
}) => {
  const userId = JSON.parse(localStorage.getItem("loginData")).userId;
  const token = JSON.parse(localStorage.getItem("loginData")).token;

  const onUpdateBooking = () => {
    const bookedId = book.bookingId;

    const updateBooking = {
      startDate: startDate,
      endDate: endDate,
      userId: userId,
      deskId: book.desk.deskId,
      daysOfMonth,
      daysOfWeek,
    };

    fetch(`${BASE_URL}/${API_VERSION}/bookings/${bookedId}`, {
      method: "PUT",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(updateBooking),
    }).then((response) => {
      response.json();
      toast.success(`Booking successfully updated!`);
      setTimeout(() => window.location.reload(), 3000);
    });
  };
  return (
    <Fragment>
      <Toaster
        position="top-center"
        reverseOrder={false}
        toastOptions={{ className: "", duration: 5000 }}
      />

      <TextField
        id="outlined-basic"
        label="Desk name"
        variant="outlined"
        readOnly
        value={book.desk.name}
      />

      <Button
        disabled={isAvailable}
        variant="contained"
        style={{ marginLeft: "auto", marginTop: "20px" }}
        onClick={onUpdateBooking}
      >
        {isAvailable ? "Desk Not Available" : "Update Booking"}
      </Button>
    </Fragment>
  );
};

export default AvailableDesks;
