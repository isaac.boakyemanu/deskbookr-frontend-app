import React, { Fragment, useState } from "react";
import Box from "@mui/material/Box";
import Container from "@material-ui/core/Container";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import SendIcon from "@mui/icons-material/Send";
import useFetch from "../shared/useFetch";
import { Autocomplete } from "@mui/material";
import { Grid } from "@material-ui/core";
import "../assets/css/style.css";
import { Toaster, toast } from "react-hot-toast";
import { BASE_URL, API_VERSION } from "../shared/config";

let selectedUsers = [];
let selectedDesks = [];

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
    },
  },
  button: {
    margin: "15px 40px",
  },
  space: {
    marginLeft: "450px",
  },
}));

function AddUsers({ startDate, endDate }) {
  const token = JSON.parse(localStorage.getItem("loginData")).token;
  const accessToken = JSON.parse(localStorage.getItem("loginData")).accessToken;
  const userEmail = JSON.parse(localStorage.getItem("loginData")).email;
  const { data: users } = useFetch(`${BASE_URL}/${API_VERSION}/users/`);
  const { data: desks } = useFetch(`${BASE_URL}/${API_VERSION}/desks/`);
  const [groupName, setGroupName] = useState("");
  const classes = useStyles();
  const [inputFields, setInputFields] = useState([{ email: "", desk: "" }]);

  const handleGroupName = (event) => {
    setGroupName(event.target.value);
  };

  let groupBooking = {
    accessToken: accessToken,
    creatorUserEmail: userEmail,
    deskIds: selectedDesks,
    users: selectedUsers,
    startDate: startDate,
    endDate: endDate,
    name: groupName,
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    fetch(`${BASE_URL}/${API_VERSION}/booking/group`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(groupBooking),
    })
      .then((res) => {
        if (res.status === 201) {
          toast.success(`Group Booking Successful !`);
          setTimeout(() => window.location.reload(), 5000);
        } else
          toast.error(
            "Unfortunately your booking was unsuccessful due to a scheduling conflict. Please try again to book a different desk"
          );
      })
      .catch((err) => {
        toast.error("An error occurred. Please make a booking again");
      });
  };

  const handleAddFields = () => {
    setInputFields([...inputFields, { email: "", desk: "" }]);
  };

  const handleRemoveFields = () => {
    const values = [...inputFields];
    values.splice(1, values.length);
    setInputFields(values);
    selectedDesks.length = 0;
    selectedUsers.length = 0;
  };

  return (
    <Fragment>
      <Toaster
        position="top-center"
        reverseOrder={false}
        toastOptions={{ duration: 5000 }}
      />
      <Container className="border">
        <Grid container spacing={1}>
          <Grid item xs={12} md={10}>
            <h3>Add Your Team</h3>
          </Grid>
          <Grid item xs={12} md={2}>
            <Button
              className={classes.button}
              variant="contained"
              color="secondary"
              onClick={handleRemoveFields}
            >
              Clear
            </Button>
          </Grid>
        </Grid>
        <form className={classes.root}>
          {inputFields.map((inputField, index) => (
            <div key={index}>
              <Grid container spacing={1}>
                <Grid item xs={12} md={4}>
                  <Autocomplete
                    id="search-bar"
                    onChange={(event, user) => {
                      selectedUsers.push(user);
                    }}
                    getOptionLabel={(users) => `${users.email}`}
                    options={users}
                    sx={{ width: 300 }}
                    isOptionEqualToValue={(option, value) =>
                      option.email === value.email
                    }
                    noOptionsText={"Email not found"}
                    renderOption={(props, users) => (
                      <Box component="li" {...props} key={users.userId}>
                        {users.email}
                      </Box>
                    )}
                    renderInput={(params) => (
                      <TextField variant="outlined" {...params} label="Email" />
                    )}
                  />
                </Grid>
                <Grid item xs={12} md={4}>
                  <Autocomplete
                    id="search-bar"
                    onChange={(event, desk) => {
                      selectedDesks.push(desk.deskId);
                    }}
                    getOptionLabel={(desks) => `${desks.name}`}
                    options={desks}
                    sx={{ width: 300 }}
                    isOptionEqualToValue={(option, value) =>
                      option.email === value.email
                    }
                    noOptionsText="Desks unavailable"
                    renderOption={(props, desks) => (
                      <Box component="li" {...props} key={desks.deskId}>
                        {desks.name}
                      </Box>
                    )}
                    renderInput={(params) => (
                      <TextField variant="outlined" {...params} label="Desk" />
                    )}
                  />
                </Grid>
                <Grid item xs={12} md={2}>
                  <Button
                    className={classes.button}
                    variant="contained"
                    color="primary"
                    onClick={handleAddFields}
                  >
                    Add
                  </Button>
                </Grid>
              </Grid>
            </div>
          ))}
        </form>
      </Container>
      <Container>
        <Grid container spacing={2}>
          <Grid item xs={12} md={10}>
            <TextField
              style={{ width: 400 }}
              className={classes.space}
              required
              name="email"
              label="Enter Group Name"
              variant="outlined"
              value={groupName}
              onChange={handleGroupName}
            />
          </Grid>
          <Grid item xs={12} md={2}>
            <Button
              style={{ padding: "15px 50px" }}
              variant="contained"
              color="primary"
              type="submit"
              disabled={
                inputFields.length === 1 || groupName.trim().length === 0
              }
              endIcon={<SendIcon />}
              onClick={handleSubmit}
            >
              Book
            </Button>
          </Grid>
        </Grid>
      </Container>
    </Fragment>
  );
}

export default AddUsers;
