import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";

export default function DialogModal({
  title,
  children,
  isOpen,
  handleClose,
  width,
  outSideClose,
  handleOk,
}) {
  return (
    <div>
      <Dialog
        open={isOpen}
        onClose={outSideClose ? handleClose : () => {}}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        maxWidth={width}
      >
        <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
        <DialogContent>{children}</DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
          <Button onClick={handleOk} autoFocus variant="contained">
            Ok
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
