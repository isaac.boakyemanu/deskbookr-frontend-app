import * as React from "react";
import { Drawer, Box } from "@mui/material";

export default function CustomDrawer({
  children,
  isOpen,
  handleClose,
  width,
  variant,
  position,
}) {
  return (
    <React.Fragment>
      <Drawer
        anchor={position ? position : "right"}
        open={isOpen}
        onClose={handleClose}
        variant={variant}
        className="right-shadow"
      >
        <Box elevation={8} sx={{ width: width ? width : 320 }}>
          {children}
        </Box>
      </Drawer>
    </React.Fragment>
  );
}
