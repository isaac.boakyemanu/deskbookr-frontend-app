import PropTypes from "prop-types";

export default function Button({
  type = "button",
  buttonText,
  onClickHandler,
  loading,
  className = "",
  children,
}) {


  return (
    <button
      type={type}
      onClick={(e) => onClickHandler(e)}
      disable={loading}
      className={className}
    >
      {children}
    </button>
  );
}

Button.propTypes = {
  type: PropTypes.string,
  buttonText: PropTypes.string,
  loading: PropTypes.bool,
  onClick: PropTypes.func,
};
