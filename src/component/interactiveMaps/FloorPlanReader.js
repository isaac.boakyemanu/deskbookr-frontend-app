import React, { Fragment, useState } from "react";
import BookingSideBar from "./BookingSideBar";
import { Container } from "@material-ui/core";
import FloorPlan from "./FloorPlan";
import "../../assets/css/booking.css";
import { func } from "../../helpers";
import { BLUE, GRAY, HIBISCUS, ORANGE } from "../../shared/constants";

const FloorPlanReader = ({
  desks,
  room,
  startDate,
  endDate,
  daysOfMonth,
  daysOfWeek,
  location,
}) => {
  const bookingData = {
    startDate,
    endDate,
    room,
    daysOfWeek,
    daysOfMonth,
    location,
  };
  const [deskNames, setDeskNames] = useState([]);
  const [deskIds, setDeskIds] = useState([]);

  const deskAvailabilityHandler = (documentElement) => {
    if (documentElement) {
      const allDesks = documentElement.querySelectorAll("ellipse");
      allDesks.forEach((desk) => {
        const deskName = desk.dataset.name;
        setTooltip(desk);
        const availableDesksNames = desks.map(({ name }) => name);

        if (availableDesksNames.includes(deskName)) {
          desk.style.cursor = "pointer";
          if (desk.style.fill === BLUE || desk.style.fill === GRAY)
            desk.style.fill = BLUE;
          desk.onclick = handleSelectedDesk;
        } else {
          desk.style.fill = ORANGE;
        }
      });
    }
  };

  const getDeskIdByName = (deskName) => {
    const desk = desks.find((desk) => desk.name === deskName);
    return desk && desk.deskId;
  };

  const setTooltip = (desk) => {
    desk.setAttribute("data-toggle", "tooltip");
    desk.setAttribute("data-placement", "top");
    desk.setAttribute("title", desk.dataset.name);
  };

  const handleSelectedDesk = (event) => {
    const deskName = event.target.dataset.name;
    const newList = [...deskNames];
    if (!newList.includes(deskName)) {
      event.target.style.fill = HIBISCUS;
      setDeskIds((ids) => [...ids, getDeskIdByName(deskName)]);
      setDeskNames((current) => [...current, deskName]);
    } else {
      event.target.style.fill = BLUE;
      setDeskNames(func.removeElementFromObject(deskNames, deskName));
      setDeskIds(
        func.removeElementFromObject(deskIds, getDeskIdByName(deskName))
      );
    }
    setIsDrawerOpen(true);
  };

  function getDocumentElement(embedding_element) {
    if (!embedding_element.contentDocument) {
      var documentElement = null;
      documentElement = embedding_element.getSVGDocument();
      return documentElement;
    }
    return embedding_element.contentDocument;
  }

  const floorMapChangeNotifier = () => {
    findSVGElements();
  };

  function findSVGElements() {
    let elms = document.querySelector("embed");
    let documentElement = getDocumentElement(elms);
    deskAvailabilityHandler(documentElement);
  }

  const [isDrawerOpen, setIsDrawerOpen] = useState(false);

  return (
    <Fragment>
      <BookingSideBar
        deskNames={deskNames}
        bookingData={bookingData}
        deskIds={deskIds}
        isOpen={isDrawerOpen}
      />

      <Container>
        {
          <FloorPlan
            room={room}
            floorMapChangeNotifier={floorMapChangeNotifier}
          />
        }
      </Container>
    </Fragment>
  );
};

export default FloorPlanReader;
