import apJollof from "../../assets/img/ap-jollof.svg";
import shOdc1 from "../../assets/img/sh-odc.svg";
import shTlc from "../../assets/img/sh-tlc.svg";
import apBanku from "../../assets/img/ap-banku.svg";
import { AP_BANKU, AP_JOLLOF, SH_ODC1, SH_TLC } from "../../shared/config";
import { useEffect, useState, useReducer } from "react";

const FloorPlan = ({ room, floorMapChangeNotifier }) => {
  const [roomFloorMap, setRoomFloorMap] = useState("");
  const [mapRender, setMapRender] = useState(false);
  const [, forceUpdate] = useReducer((x) => x + 1, 0);

  setTimeout(() => {
    setMapRender(true);
  }, 500);
  useEffect(() => {
    if (room === AP_BANKU) setRoomFloorMap(apBanku);
    else if (room === AP_JOLLOF) setRoomFloorMap(apJollof);
    else if (room === SH_ODC1) setRoomFloorMap(shOdc1);
    else if (room === SH_TLC) setRoomFloorMap(shTlc);

    floorMapChangeNotifier();
    if (mapRender) forceUpdate();
  }, [room, roomFloorMap, floorMapChangeNotifier, mapRender]);

  return <embed src={roomFloorMap} type="image/svg+xml" />;
};

export default FloorPlan;
