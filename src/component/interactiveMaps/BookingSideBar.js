import * as React from "react";
import { Toaster, toast } from "react-hot-toast";
import { BASE_URL, API_VERSION } from "../../shared/config";
import { useNavigate } from "react-router-dom";
import {
  Button,
  TextField,
  ListItemIcon,
  ListItemText,
  MenuItem,
  Box,
  Divider,
  List,
  ListItem,
  Select,
  Checkbox,
  Grid,
  Typography,
} from "@mui/material";
import { CustomDrawer } from "../ui";
import { Chip } from "@material-ui/core";
import {
  LocationCity,
  MeetingRoom,
  AccessTime,
  EventRepeat,
  EventSeat,
  Groups,
  People,
} from "@mui/icons-material";
import { fetch as fetcher } from "../../helpers";

const BookingSideBar = ({ deskNames, bookingData, deskIds, isOpen }) => {
  const userData = JSON.parse(localStorage.getItem("loginData"));
  const currentUserEmail = userData.email;
  const initialUsersState = {
    username: `${userData.firstName} ${userData.lastName}`,
    email: userData.email,
  };
  const [users, setUsers] = React.useState([initialUsersState]);
  const navigate = useNavigate();
  const [setIsDrawerOpen] = React.useState(false);
  const [group, setGroup] = React.useState([]);

  const [members, setMembers] = React.useState([]);
  const [groups, setGroups] = React.useState();

  const bookingObj = {
    groupName: group.map((group) => group.name).toString(),
    groupEmail: group.map((group) => group.email).toString(),
    startDate: bookingData.startDate,
    endDate: bookingData.endDate,
    daysOfWeek: bookingData.daysOfWeek,
    daysOfMonth: bookingData.daysOfMonth,
    deskIDs: deskIds,
    users: users,
  };

  const groupsUrl = `${BASE_URL}/${API_VERSION}/groups?userEmail=${currentUserEmail}`;
  const membersUrl = `${BASE_URL}/${API_VERSION}/users?groupEmail=${
    group.length > 0 && group.map((group) => group.email)
  }`;

  React.useEffect(() => {
    currentUserEmail.length > 0 &&
      fetcher.get(groupsUrl).then((data) => {
        setGroups(data);
      });

    group.length > 0 &&
      fetcher.get(membersUrl).then((data) => {
        setMembers(data);
      });
  }, [groupsUrl, membersUrl, group, currentUserEmail]);

  const handleBooking = () => {
    validateDeskAndUsersSize()
      ? toast.error("Selected Desks must match the number of Users")
      : fetch(`${BASE_URL}/${API_VERSION}/bookings`, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${userData.token}`,
          },
          body: JSON.stringify(bookingObj),
        }).then((res) => {
          if (res.status === 201) {
            toast.success(`${deskNames} booked successfully!`);
            setTimeout(() => navigate("/my-bookings"), 2000);
          } else if (res.status === 401) {
            localStorage.clear();
            navigate("/");
          } else
            toast.error(
              "Unfortunately your booking was unsuccessful due to a scheduling conflict. Please try again to book a different desk"
            );
        });
  };

  const validateDeskAndUsersSize = () => {
    return deskIds.length < users.length || deskIds.length > users.length;
  };

  const handleGroupSelect = (event) => {
    const selectedGroupName = event.target.value;

    groups
      .filter((group) => group.groupName === selectedGroupName)
      .forEach((group) =>
        setGroup([
          {
            name: selectedGroupName,
            email: group.email,
          },
        ])
      );
  };

  const handleUsersSelect = (event) => {
    const username = event.target.value[0].username;
    const email = event.target.value[0].email;
    setUsers(
      users.some((user) => user.username === username)
        ? (users) => users.filter((user) => user.username !== username)
        : (users) => [
            ...users,
            {
              username: username,
              email: email,
            },
          ]
    );
  };

  return (
    <div>
      <CustomDrawer
        handleClose={() => setIsDrawerOpen(false)}
        isOpen={isOpen}
        variant="persistent"
      >
        <List sx={{ marginTop: "8rem" }}>
          <ListItem>
            <ListItemIcon>
              <LocationCity />
            </ListItemIcon>
            <ListItemText primary="Location" secondary={bookingData.location} />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <MeetingRoom />
            </ListItemIcon>
            <ListItemText primary="Room" secondary={bookingData.room} />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <AccessTime />
            </ListItemIcon>
            <ListItemText
              primary="Date"
              secondary={`${bookingData.startDate} - ${bookingData.endDate}`}
            />
          </ListItem>
          <ListItem>
            <ListItemIcon>
              <EventRepeat />
            </ListItemIcon>
            <ListItemText
              primary={`Repeat ${
                bookingData.daysOfWeek
                  ? " - Weekly"
                  : bookingData.daysOfMonth
                  ? " - Monthly"
                  : ""
              }`}
              secondary={
                bookingData.daysOfWeek ? (
                  <Typography>{bookingData.daysOfWeek}</Typography>
                ) : bookingData.daysOfMonth ? (
                  <Typography>{bookingObj.daysOfMonth}</Typography>
                ) : (
                  "None"
                )
              }
            />
          </ListItem>
        </List>
        <Divider />
        <List>
          <ListItem>
            <ListItemIcon>
              <EventSeat />
            </ListItemIcon>
            <ListItemText
              primary={`Desk (${deskNames.length})`}
              secondary={deskNames.map((desk) => (
                <React.Fragment key={desk}>
                  <span>{desk}</span>
                  <br />
                </React.Fragment>
              ))}
            />
          </ListItem>
        </List>
        {deskNames.length > 1 && (
          <React.Fragment>
            <Divider />
            <List>
              <ListItem>
                <ListItemIcon>
                  <Groups />
                </ListItemIcon>
                <ListItemText
                  primary={
                    <Box sx={{ minWidth: 220 }}>
                      <TextField
                        label="Select Group"
                        select
                        fullWidth
                        onChange={handleGroupSelect}
                        required
                        value={
                          group.length > 0
                            ? group.map((group) => group.name)
                            : ""
                        }
                        size="small"
                      >
                        {groups.map((group) => (
                          <MenuItem key={group.email} value={group.groupName}>
                            {group.groupName}
                          </MenuItem>
                        ))}
                      </TextField>
                    </Box>
                  }
                />
              </ListItem>
            </List>
            <Divider />
            <List>
              <ListItem>
                <ListItemIcon>
                  <People />
                </ListItemIcon>
                <ListItemText
                  primary={
                    <Box sx={{ minWidth: 220 }}>
                      <Select
                        label="Add Users"
                        multiple
                        value={[]}
                        onChange={handleUsersSelect}
                        fullWidth
                        size="small"
                      >
                        {members
                          .filter((member) => member.email !== currentUserEmail)
                          .map((member) => (
                            <MenuItem
                              key={member.email}
                              value={{
                                username: member.username,
                                email: member.email,
                              }}
                            >
                              <Checkbox
                                checked={users.some(
                                  (user) => user.username === member.username
                                )}
                              />
                              <ListItemText primary={member.username} />
                            </MenuItem>
                          ))}
                      </Select>
                    </Box>
                  }
                />
              </ListItem>
              <div className="ml-3">
                {users.map((user) => {
                  return (
                    <Chip
                      className="m-1"
                      key={user.email}
                      label={
                        user.email === currentUserEmail ? "You" : user.username
                      }
                    />
                  );
                })}
              </div>
            </List>
          </React.Fragment>
        )}
        <Divider sx={{ marginBottom: "2rem" }} />
        <Grid container spacing={2} className="text-center pl-3 pr-3">
          <Grid item xs={6} md={6}>
            <Button
              variant="outlined"
              size="medium"
              fullWidth
              onClick={() => setIsDrawerOpen(false)}
            >
              Close
            </Button>
          </Grid>
          <Grid item xs={6} md={6}>
            <Button
              size="medium"
              fullWidth
              variant="contained"
              onClick={handleBooking}
            >
              Book
            </Button>
          </Grid>
        </Grid>
      </CustomDrawer>
      <Toaster
        position="top-center"
        reverseOrder={false}
        toastOptions={{ className: "", duration: 5000 }}
      />
    </div>
  );
};

export default BookingSideBar;
