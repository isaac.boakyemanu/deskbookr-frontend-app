import React from 'react';


function LoginFooter() {
    return (
        <p className='copyright text-center'>Copyright © 2022 Turntabl. All Rights Reserved.</p>
    );
}
export default LoginFooter;