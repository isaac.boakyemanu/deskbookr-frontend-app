import * as React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Avatar from "@material-ui/core/Avatar";
import Logo from "../shared/Logo";
import ProfileMenu from "./ProfileMenu";
import ViewButton from "../shared/viewButton";

const useStyles = makeStyles((theme) => ({
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: { flexGrow: 1 },
  avatar: {
    marginRight: "0.7rem",
    backgroundColor: "#ffa500",
  },
}));

const Navbar = () => {
  const classes = useStyles();
  const data = JSON.parse(localStorage.getItem("loginData"));

  return (
    <AppBar position="fixed">
      <Toolbar>
        <IconButton
          edge="start"
          color="inherit"
          aria-label="menu"
          className={classes.menuButton}
        >
          <Logo />
        </IconButton>
        <Typography variant="h6" className={classes.title}>
          {" "}
          BookingApp{" "}
        </Typography>
        <ViewButton />
        <Avatar alt={data.firstName} className={classes.avatar}>
          {data.firstName.charAt(0)}
        </Avatar>
        <Typography variant="h6"> {data.firstName}</Typography>
        <ProfileMenu />
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
