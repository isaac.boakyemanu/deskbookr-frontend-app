import { render } from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Booking from "./pages/Booking";
import Login from "./pages/Login";
import NotFound from "./pages/NotFound";
import UserBookings from "./pages/UserBookings";
import ProtectedRoutes from "./shared/ProtectedRoutes";

const rootElement = document.getElementById("root");

render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<Login />} />
      <Route element={<ProtectedRoutes />}>
        <Route path="/booking" element={<Booking />} />
        <Route path="/my-bookings" element={<UserBookings />} />
      </Route>
      <Route path="*" element={<NotFound />} />
    </Routes>
  </BrowserRouter>,
  rootElement
);
