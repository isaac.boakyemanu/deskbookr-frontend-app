import { Container } from "@mui/material";
import "../assets/css/style.css";
import apbanku from "../assets/img/apbanku.png";
import apjollof from "../assets/img/apjollof.png";
import shodc from "../assets/img/shodc.png";
import shtlc from "../assets/img/shtlc.png";

const FloorPlan = ({ room }) => {
  const imageSize = {
    width: "100%",
    height: "100%",
  };
  let showImage = <p></p>;
  if (room === "AP-BK") {
    showImage = (
      <img
        alt=""
        src={apbanku}
        width={imageSize.width}
        height={imageSize.height}
      />
    );
  } else if (room === "AP-J") {
    showImage = (
      <img
        alt=""
        src={apjollof}
        width={imageSize.width}
        height={imageSize.height}
      />
    );
  } else if (room === "SH-ODC1") {
    showImage = (
      <img
        alt=""
        src={shodc}
        width={imageSize.width}
        height={imageSize.height}
      />
    );
  } else {
    showImage = (
      <img
        alt=""
        src={shtlc}
        width={imageSize.width}
        height={imageSize.height}
      />
    );
  }

  return <Container>{showImage}</Container>;
};

export default FloorPlan;
