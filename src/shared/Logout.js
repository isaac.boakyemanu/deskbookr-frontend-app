import { useNavigate } from "react-router-dom";
import MenuItem from "@material-ui/core/MenuItem";

const Logout = () => {
  const navigate = useNavigate();

  const logoutHandler = (e) => {
    localStorage.clear();
    e.preventDefault();
    navigate("/");
  };
  return (
    <div>
      <MenuItem onClick={logoutHandler}>Logout</MenuItem>
    </div>
  );
};

export default Logout;
