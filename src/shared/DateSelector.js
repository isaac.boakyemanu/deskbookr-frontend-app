import AdapterDateFns from "@mui/lab/AdapterDateFns";
import { LocalizationProvider, MobileDatePicker } from "@mui/lab";
import { TextField, FormControl } from "@mui/material";
import { useState } from "react";
import React from "react";
import'../assets/css/style.css';

const DateSelector = ({ label, dateSelectHandler, isDisabled, startDate }) => {
  const [date, setDate] = useState(null);

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <FormControl fullWidth>
        <label>Select {label + " Date"}</label>
        <MobileDatePicker
          className="select"
          label={label}
          value={date}
          inputFormat="dd/MM/yyyy"
          minDate={startDate ? new Date(startDate) : new Date()}
          disabled={isDisabled}
          onChange={(date, dateLabel = label) => {
            dateSelectHandler(date, dateLabel);
            setDate(date);
          }}
          renderInput={(params) => <TextField fullWidth {...params} />}
        />
      </FormControl>
    </LocalizationProvider>
  );
};

export default DateSelector;
