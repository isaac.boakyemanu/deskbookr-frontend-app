import { FormControl } from "@mui/material";
import { useState } from "react";
import "../assets/css/style.css";
import { AP_BANKU, AP_JOLLOF, SH_ODC1, SH_TLC } from "./config";

const Room = ({roomChangeHandler, location}) => {
  const [room, setRoom] = useState();

  return (
   <div>
     <FormControl fullWidth>
      <label>Select a Room</label>
      <select 
        onChange={(e) => {
        roomChangeHandler(e);
        setRoom(e.target.value);
}}
        className="room"
        value={room}
        name="room"
      >

        <option value="">Select a Room</option>
        {location==="SH" ? 
        (
        <>
        <option value={SH_ODC1}>ODC1 Room</option>
        <option value={SH_TLC}>TLC Room</option>
        </>
        ) : location === "AP" ?
        (
        <>
        <option value={AP_BANKU}>Banku Room</option>
        <option value={AP_JOLLOF}>Jollof Room</option>
        </> 
        ) :
        (

        <>
        <option value={SH_ODC1}>ODC1 Room</option>
        <option value={SH_TLC}>TLC Room</option>
        <option value={AP_BANKU}>Banku Room</option>
        <option value={AP_JOLLOF}>Jollof Room</option>
        </>
        )
       }

      </select>
    </FormControl>
   </div>
   
  );
};
export default Room;