import '../assets/css/style.css';
import tt_logo from "../assets/img/logo.png";

const Logo = () => {
 
  return <img src={tt_logo} className='logo' alt="Turntabl Logo" />;
};

export default Logo;
