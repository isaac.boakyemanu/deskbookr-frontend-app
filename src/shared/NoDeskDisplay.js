import { Container, Typography } from "@mui/material";
import  '../assets/css/style.css';

const NoDeskDisplay = ()=>{
    return(
        <div className="bookingCard">
            <Container>
            <Typography  variant="h4" component="h6" align = "center">
                No desks available for the selected dates
            </Typography>
        </Container>
        </div>
    );
}
export default NoDeskDisplay;
