import { FormControl, Checkbox, FormControlLabel } from "@mui/material";
import { Fragment, useState } from "react";
import "../assets/css/style.css";
import { WEEKLY, MONTHLY } from "./config";
import { DialogModal } from "../component/ui";

const Recurring = ({ endDate, recurringHandler }) => {
  const [dayOfWeek, setDayOfWeek] = useState("");
  const [dayOfMonth, setDayOfMonth] = useState("");

  const [repeatType, setRepeatType] = useState(null);
  const [repeatData, setRepeatData] = useState([]);

  const dataObj = {
    dayOfMonth,
    dayOfWeek,
  };

  const getRepeatData = (e) => {
    const data = e.target.value;
    if (e.target.checked) {
      repeatData.push(data);
    } else {
      let index = repeatData.indexOf(data);
      if (index > -1) {
        repeatData.splice(index, 1);
      }
    }
    if (repeatType === WEEKLY) {
      setDayOfWeek(repeatData.toLocaleString());
    } else {
      setDayOfMonth(repeatData.toLocaleString());
    }
  };

  const handleRepeatDropdown = (e) => {
    setRepeatData([]);
    setDayOfMonth("");
    setDayOfWeek("");
    let repeat = e.target.value;
    if (repeat === WEEKLY) {
      setRepeatType(WEEKLY);
      setIsOpen(true);
      setDayOfWeek("");
    } else if (repeat === MONTHLY) {
      setDayOfMonth("");
      setRepeatType(MONTHLY);
      setIsOpen(true);
    } else {
      setRepeatData([]);
      setRepeatType(null);
      setIsOpen(false);
    }
  };

  const handleDialogClose = () => {
    setIsOpen(false);
  };

  const handleOk = (e) => {
    handleDialogClose();
    recurringHandler(dataObj);
  };

  const daysOfTheWeek = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];

  const daysOfTheMonth = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
    22, 23, 24, 25, 26, 27, 28,
  ];

  const [isOpen, setIsOpen] = useState(false);
  return (
    <Fragment>
      <FormControl fullWidth>
        <label>Repeat {repeatType}</label>
        <select
          className="select"
          disabled={!endDate}
          onChange={handleRepeatDropdown}
          onSelect={handleRepeatDropdown}
        >
          <option value="None">None</option>
          <option value="weekly">Weekly</option>
          <option value="monthly">Monthly</option>
        </select>
      </FormControl>

      <DialogModal
        isOpen={isOpen}
        title={"Select Booking Days"}
        width={"md"}
        handleClose={handleDialogClose}
        outSideClose={false}
        handleOk={handleOk}
      >
        {repeatType === MONTHLY ? (
          <Fragment>
            {daysOfTheMonth.map((day, index) => (
              <FormControlLabel
                key={index}
                onChange={getRepeatData}
                control={<Checkbox name="monthDay" value={day} />}
                label={day}
              />
            ))}
          </Fragment>
        ) : (
          <Fragment>
            {daysOfTheWeek.map((day, index) => (
              <FormControlLabel
                key={index}
                onChange={getRepeatData}
                control={<Checkbox name="dayName" value={day} />}
                label={day}
              />
            ))}
          </Fragment>
        )}
      </DialogModal>
    </Fragment>
  );
};
export default Recurring;
