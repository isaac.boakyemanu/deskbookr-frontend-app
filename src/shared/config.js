export const BASE_URL = process.env.REACT_APP_BASE_URL
  // process.env.NODE_ENV === "production"
  //   ? process.env.REACT_APP_PROD_BASE_URL
  //   : process.env.REACT_APP_DEV_BASE_URL;

export const API_VERSION = "api/v1";

export const AP_BANKU = "AP-BK";
export const AP_JOLLOF = "AP-J";
export const SH_ODC1 = "SH-ODC1";
export const SH_TLC = "SH-TLC";

export const WEEKLY = "weekly";
export const MONTHLY = "monthly";
