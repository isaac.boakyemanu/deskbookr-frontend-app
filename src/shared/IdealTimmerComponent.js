import { useIdleTimer } from "react-idle-timer";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import Button from "@mui/material/Button";
import { Grid } from "@material-ui/core";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";

function IdealTimmerComponent() {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const handleOpen = () => setModalIsOpen(true);
  const handleClose = () => setModalIsOpen(false);
  const navigate = useNavigate();

  const logoutHandler = () => {
    localStorage.clear();
    navigate("/");
  };

  const onIdle = () => {
    handleOpen();
  };

  useIdleTimer({
    onIdle,
    timeout: 1000 * 60 * 60,
    promptTimeout: 0,
    events: [
      "mousemove",
      "keydown",
      "wheel",
      "DOMMouseScroll",
      "mousewheel",
      "mousedown",
      "touchstart",
      "touchmove",
      "MSPointerDown",
      "MSPointerMove",
      "visibilitychange",
    ],
    immediateEvents: [],
    debounce: 0,
    throttle: 0,
    eventsThrottle: 200,
    element: document,
    startOnMount: true,
    startManually: false,
    stopOnIdle: false,
    crossTab: false,
    syncTimers: 0,
  });

  return (
    <Modal
      open={modalIsOpen}
      onClose={handleClose}
      aria-labelledby="modal-modal-title"
      aria-describedby="modal-modal-description"
    >
      <Box className="updateStyle">
        <Typography
          id="modal-modal-title"
          variant="h6"
          component="h2"
          className="heading"
        >
          Your session has expired. Login to continue
        </Typography>
        <Grid container spacing={3}>
          <Button
            onClick={logoutHandler}
            variant="contained"
            style={{ marginLeft: "auto" }}
          >
            Ok
          </Button>
        </Grid>
      </Box>
    </Modal>
  );
}

export default IdealTimmerComponent;
