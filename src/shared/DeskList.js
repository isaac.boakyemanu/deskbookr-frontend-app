import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Button,
  Container,
  Typography,
} from "@mui/material";
import { Toaster, toast } from "react-hot-toast";
import "../assets/css/style.css";
import { BASE_URL, API_VERSION } from "./config";
import { useNavigate } from "react-router-dom";
import NoDeskDisplay from "./NoDeskDisplay";

const DeskList = ({
  desks,
  title,
  startDate,
  endDate,
  daysOfWeek,
  daysOfMonth,
}) => {
  const navigate = useNavigate();
  const userData = JSON.parse(localStorage.getItem("loginData"));
  const userId = userData.userId;

  const handleSubmit = (deskId, deskName) => {
    const bookingObject = {
      startDate,
      endDate,
      userId,
      deskId,
      daysOfWeek,
      daysOfMonth,
    };

    fetch(`${BASE_URL}/${API_VERSION}/bookings`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userData.token}`,
      },
      body: JSON.stringify(bookingObject),
    })
      .then((res) => {
        if (res.status === 201) {
          toast.success(`${deskName} booked successfully!`);
          setTimeout(() => navigate("/my-bookings"), 5000);
        } else if (res.status === 401) {
          localStorage.clear();
          navigate("/");
        } else
          toast.error(
            "Unfortunately your booking was unsuccessful due to a scheduling conflict. Please try again to book a different desk"
          );
      })
      .catch((err) => {
        toast.error("An error occurred. Please make a booking again");
      });
  };

  return (
    <div>
      <Toaster
        position="top-center"
        reverseOrder={false}
        toastOptions={{ className: "", duration: 5000 }}
      />

      <Container>
        {desks.length > 0 ? (
          <div className="bookingCard">
            <Typography className="title-sm">{title}</Typography>
            <TableContainer className="table-style">
              <Table xs={12}>
                <TableHead>
                  <TableRow>
                    <TableCell>Desk Name</TableCell>
                    <TableCell>Capacity</TableCell>
                    <TableCell>Location</TableCell>
                    <TableCell>Action</TableCell>
                  </TableRow>
                </TableHead>

                <TableBody>
                  {desks.map((desk, index) => (
                    <TableRow key={`table-row ${index}`}>
                      <TableCell>{desk.name}</TableCell>
                      <TableCell>{desk.capacity}</TableCell>
                      <TableCell>
                        {desk.location === "AP"
                          ? "Advantage Place"
                          : desk.location === "SH"
                          ? "Sonnidom  House"
                          : null}
                      </TableCell>
                      <TableCell>
                        <Button
                          onClick={() => {
                            handleSubmit(desk.deskId, desk.name);
                          }}
                          variant="contained"
                        >
                          Book
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </div>
        ) : (
          <NoDeskDisplay />
        )}
      </Container>
    </div>
  );
};

export default DeskList;
