import { Typography } from "@mui/material";
import * as React from "react";
import { Link } from "react-router-dom";
import "../assets/css/style.css";

const ViewButton = () => {
  return (
    <div>
      <Typography mr={8} variant="h5">
        <Link to="/my-bookings" className="viewButton mr-4">
          View Bookings
        </Link>
        <Link to="/booking" className="viewButton mr-4">
          Make Booking
        </Link>
      </Typography>
    </div>
  );
};
export default ViewButton;
