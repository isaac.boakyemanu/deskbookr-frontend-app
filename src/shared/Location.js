import { FormControl } from "@mui/material";
import { useState, useEffect } from "react";
import "../assets/css/style.css";
import { SH_ODC1, SH_TLC, AP_BANKU, AP_JOLLOF } from "./config";

const Location = ({
  locationChangeHandler,
  disable = false,
  room,
  locationReceiver,
}) => {
  const [location, setLocation] = useState("");

  useEffect(() => {
    if (room === AP_BANKU || room === AP_JOLLOF) {
      setLocation("AP");
      locationReceiver("AP");
    }

    if (room === SH_ODC1 || room === SH_TLC) {
      setLocation("SH");
      locationReceiver("SH");
    }
  }, [room, locationReceiver]);

  return (
    <FormControl fullWidth>
      <label>Select a Location</label>
      <select
        disabled={disable ? true : false}
        className="select"
        onChange={(e) => {
          locationChangeHandler(e);
          setLocation(e.target.value);
        }}
        value={location}
        name="location"
      >
        <option value="">Select a Location</option>
        <option value="SH">Sonnidom House</option>
        <option value="AP">Advantage Place</option>
      </select>
    </FormControl>
  );
};
export default Location;
