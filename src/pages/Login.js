import React from "react";
import "../assets/css/style.css";
import Title from "../component/LoginTitle";
import SigninButton from "../component/SigninButton";
import Logo from "../shared/Logo";
import LoginFooter from "../component/LoginFooter";
import GoogleLogin from "react-google-login";
import { useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import { BASE_URL, API_VERSION } from "../shared/config";

function Login() {
  const navigate = useNavigate();

  const [loginData] = useState(
    localStorage.getItem("loginData")
      ? JSON.parse(localStorage.getItem("loginData"))
      : null
  );

  const handleLogin = async (googleData) => {
    const data = {
      firstName: googleData.profileObj.givenName,
      lastName: googleData.profileObj.familyName,
      email: googleData.profileObj.email,
      imageUrl: googleData.profileObj.imageUrl,
      userId: null,
      token: googleData.tokenId,
      accessToken: googleData.accessToken
    };

    const userObj = {
      username:
        googleData.profileObj.givenName +
        " " +
        googleData.profileObj.familyName,
      email: googleData.profileObj.email,
    };
    const bookingObj = {
      location: null,
      startDate: null,
      endDate: null,
      userId: null,
      deskId: null,
    };
    fetch(`${BASE_URL}/${API_VERSION}/authenticate`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${data.token}`,
      },
      body: JSON.stringify(userObj),
    })
      .then((response) => response.json())
      .then((id) => {
        data.userId = id;
        bookingObj.userId = id;
        localStorage.setItem("loginData", JSON.stringify(data));
        localStorage.setItem("bookingData", JSON.stringify(bookingObj));
        localStorage.setItem("isAuthenticated", true);
        navigate("/booking");
      });
  };

  const handleFailure = (response) => {
    Error(response);
  };

  return (
    <div>
      <main>
        <section className="login">
          <div className="container">
            <div className="row">
              {loginData ? (
                <Navigate to="/booking" />
              ) : (
                <div className="col-lg-5 center-card">
                  <Logo />
                  <Title />
                  <GoogleLogin
                    clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
                    buttonText="Sign in with Google"
                    render={(renderProps) => (
                      <span
                        onClick={renderProps.onClick}
                        disabled={renderProps.disabled}
                      >
                        <SigninButton />
                      </span>
                    )}
                    onSuccess={handleLogin}
                    onFailure={handleFailure}
                    cookiePolicy={"single_host_origin"}
                  />
                </div>
              )}
            </div>
            <LoginFooter />
          </div>
        </section>
      </main>
    </div>
  );
}

export default Login;
