import { Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import '../assets/css/style.css';

const NotFound = () => {
    return (
        <div className="notFound">
           <Typography variant="h2">Page Not Found</Typography>
            <Link to='/'>Return to Home Page</Link>
        </div>
    );
}
 
export default NotFound;