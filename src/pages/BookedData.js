import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from "@mui/material";
import CancelBooking from "./CancelBooking";
import UpdateBooking from "./UpdateBooking";
import { format } from "date-fns";
import React, { Fragment } from "react";

const BookedData = ({ bookings }) => {
  return (
    <Fragment>
      <TableContainer>
        <Table xs={12} p={5}>
          <TableHead>
            <TableRow>
              <TableCell>Desk Name</TableCell>
              <TableCell>Location</TableCell>
              <TableCell>Start Date</TableCell>
              <TableCell>End Date</TableCell>
              <TableCell>Status</TableCell>
              <TableCell>Repeat</TableCell>
              <TableCell>Action</TableCell>
            </TableRow>
          </TableHead>

          <TableBody style={{ width: 1000 }}>
            {bookings
              .sort((a, b) => (a.bookingId < b.bookingId ? 1 : -1))
              .map((book) => (
                <TableRow key={book.bookingId}>
                  <TableCell>{book.desk.name}</TableCell>
                  <TableCell>
                    {book.desk.location === "AP"
                      ? "Advantage Place"
                      : book.desk.location === "SH"
                      ? "Sonnidom House"
                      : null}
                  </TableCell>
                  <TableCell>
                    {format(new Date(book.startDate), "dd-MM-yyyy")}
                  </TableCell>
                  <TableCell>
                    {format(new Date(book.endDate), "dd-MM-yyyy")}
                  </TableCell>
                  <TableCell>{book.status}</TableCell>
                  <TableCell>
                    {book.daysOfWeek.length > 1
                      ? "WEEKLY"
                      : book.daysOfMonth.length > 1
                      ? "MONTHLY"
                      : "NONE"}
                  </TableCell>
                  <TableCell>
                    <UpdateBooking book={book} />
                    <CancelBooking
                      bookingId={book.bookingId}
                      bookingStatus={book.status}
                    />
                  </TableCell>
                </TableRow>
              ))}
            {bookings.length <= 0 && (
              <TableRow>
                <TableCell colSpan={6} align="center">
                  No Bookings..
                </TableCell>
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </Fragment>
  );
};

export default BookedData;
