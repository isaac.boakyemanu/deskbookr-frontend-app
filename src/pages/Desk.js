import DeskList from "../shared/DeskList";
import useFetch from "../shared/useFetch";
import Container from "@mui/material/Container";
import { BASE_URL, API_VERSION } from "../shared/config";

const Desk = ({
  location,
  startDate,
  endDate,
  room,
  dayOfMonth,
  dayOfWeek,
}) => {
  const dataObj = {
    startDate,
    endDate,
    room,
    dayOfMonth,
    dayOfWeek,
  };
  
  const {
    data: desks,
    isPending,
    error,
  } = useFetch(`${BASE_URL}/${API_VERSION}/desks?` + new URLSearchParams(dataObj).toString());
  return (
    <div className="home">
      {error && <div>{error}</div>}
      {isPending && <Container align="center">Loading....</Container>}
      {location ? (
        <div>
          {desks && (
            <DeskList
              desks={desks}
              location={location}
              startDate={startDate}
              endDate={endDate}
              daysOfWeek={dayOfMonth}
              daysOfMonth={dayOfMonth}
              title="Available Desks"
            />
          )}
        </div>
      ) : null}
    </div>
  );
};

export default Desk;
