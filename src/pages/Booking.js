import "../assets/css/style.css";
import { Grid } from "@material-ui/core";
import { Container, Box, Button, CircularProgress } from "@mui/material";
import { format } from "date-fns";
import { Fragment, useState } from "react";
import { useSearchParams } from "react-router-dom";
import IdealTimmerComponent from "../shared/IdealTimmerComponent";
import Navbar from "../component/Navbar";
import DateSelector from "../shared/DateSelector";
import Location from "../shared/Location";
import Room from "../shared/Room";
import FloorPlanReader from "../component/interactiveMaps/FloorPlanReader";
import Recurring from "../shared/Recurring";
import { BASE_URL, API_VERSION } from "../shared/config";
import { fetch } from "../helpers";
import DeskList from "../shared/DeskList";

const Booking = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [location, setLocation] = useState("");
  const [room, setRoom] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");
  const [loading, setLoading] = useState(false);
  const [daysOfWeek, setDaysOfWeek] = useState("");
  const [daysOfMonth, setDaysOfMonth] = useState("");

  const locationChangeHandler = (e) => {
    setSearchParams(searchParams);
    setLocation(e.target.value);
  };

  const url = `${BASE_URL}/${API_VERSION}/desks?`;

  const bookingData = { startDate, endDate, room, daysOfWeek, daysOfMonth };

  const [desks, setDesks] = useState([]);

  const roomChangeHandler = (e) => {
    setSearchParams(searchParams);
    setRoom(e.target.value);
  };

  const dateSelectHandler = (date, label) => {
    date = date ? format(date, "yyyy-MM-dd") : null;
    if (label === "Start") {
      setStartDate(date);
    } else {
      setEndDate(date);
    }
  };

  const locationReceiver = (location) => {
    setLocation(location);
  };

  const recurringHandler = (obj) => {
    setDaysOfMonth(obj.dayOfMonth);
    setDaysOfWeek(obj.dayOfWeek);
  };

  const handleTriggerFetch = () => {
    setLoading(true);
    const res = fetch.get(url + new URLSearchParams(bookingData).toString());
    res.then((data) => {
      setDesks(data);
      setLoading(data.length > 0 && false);
    });
  };
  return (
    <Fragment>
      <Navbar />
      <Container sx={{ marginTop: "10rem" }}>
        <div className="bookingCard">
          <Grid container spacing={3}>
            <Grid item xs={12} md={3}>
              <Location
                locationChangeHandler={locationChangeHandler}
                room={room}
                locationReceiver={locationReceiver}
              />
            </Grid>
            <Grid item xs={12} md={2}>
              <Room roomChangeHandler={roomChangeHandler} location={location} />
            </Grid>
            <Grid item xs={12} md={2}>
              <DateSelector
                label="Start"
                dateSelectHandler={dateSelectHandler}
                isDisabled={!room}
              />
            </Grid>
            <Grid item xs={12} md={2}>
              <DateSelector
                label="End"
                dateSelectHandler={dateSelectHandler}
                isDisabled={!startDate}
                startDate={startDate}
              />
            </Grid>
            <Grid item xs={12} md={2}>
              <Recurring
                endDate={endDate}
                recurringHandler={recurringHandler}
              />
            </Grid>
            <Grid item xs={12} md={1}>
              <Button
                variant="contained"
                style={{ padding: "1rem", marginTop: "2rem" }}
                onClick={handleTriggerFetch}
                disabled={!endDate}
              >
                ok
              </Button>
            </Grid>
          </Grid>
        </div>

        {loading && (
          <Box className="d-flex justify-content-center align-items-center">
            <CircularProgress />
          </Box>
        )}
      </Container>
      {desks.length > 0 && (
        <Fragment>
          <FloorPlanReader
            room={room}
            desks={desks}
            startDate={startDate}
            endDate={endDate}
            daysOfMonth={daysOfMonth}
            daysOfWeek={daysOfWeek}
            location={location}
          />
          <DeskList
            desks={desks}
            startDate={startDate}
            endDate={endDate}
            room={room}
            daysOfMonth={daysOfMonth}
            daysOfWeek={daysOfWeek}
            title="Available Desks"
          />
        </Fragment>
      )}

      <IdealTimmerComponent />
    </Fragment>
  );
};

export default Booking;
