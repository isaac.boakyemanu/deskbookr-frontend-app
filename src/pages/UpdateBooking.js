import React, { Fragment, useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import { Grid } from "@material-ui/core";
import "../assets/css/updatebooking.css";
import {
  FormControl,
  TextField,
  Checkbox,
  FormControlLabel,
} from "@mui/material";
import { LocalizationProvider, MobileDatePicker } from "@mui/lab";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import AvailableDesks from "../component/AvailableDesks";
import { BASE_URL, API_VERSION, MONTHLY, WEEKLY } from "../shared/config";
import { format } from "date-fns";
import { fetch, func } from "../helpers";

export default function UpdateBooking({ book }) {
  const [desks, setDesks] = useState([]);
  let [updateStartDate, setUpdateStartDate] = useState(book.startDate);
  let [updateEndDate, setUpdateEndDate] = useState(book.endDate);
  const [isAvailable, setIsAvailable] = useState(true);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [daysOfMonth, setDaysOfMonth] = useState(book.daysOfMonth);
  const [daysOfWeek, setDaysOfWeek] = useState(book.daysOfWeek);
  const handleClose = () => setModalIsOpen(false);

  const [recurringDays, setRecurringDays] = useState([]);

  const room = book.desk.name.substring(0, 5);

  const url = `${BASE_URL}/${API_VERSION}/desks?`;

  const selectStartDateHandler = (date) => {
    date = date ? format(date, "yyyy-MM-dd") : null;
    setUpdateStartDate(date);
  };

  const selectEndDateHandler = (date) => {
    date = date ? format(date, "yyyy-MM-dd") : null;
    setUpdateEndDate(date);
  };

  const bookedDaysOfWeek =
    book.daysOfWeek.length > 0 && book.daysOfWeek.split(",");
  const bookedDaysOfMonth =
    book.daysOfMonth.length > 0 && book.daysOfMonth.split(",");

  useEffect(() => {
    const bookingData = {
      startDate: updateStartDate,
      endDate: updateEndDate,
      room,
      daysOfWeek,
      daysOfMonth,
    };

    const res = fetch.get(url + new URLSearchParams(bookingData).toString());
    res.then((data) => setDesks(data));
  }, [url, updateEndDate, updateStartDate, room, daysOfMonth, daysOfWeek]);

  const handleOpen = () => {
    setModalIsOpen(true);
    setRecurringDays(
      func.getArrayWithData(bookedDaysOfMonth, bookedDaysOfWeek)
    );
  };

  const checkDeskAvailability = () => {
    var check = !desks.map((desk) => desk.deskId).includes(book.desk.deskId);
    setIsAvailable(check);
  };

  const getRepeatData = (e) => {
    const day = e.target.value;
    if (e.target.checked) {
      recurringDays.push(day);
    } else {
      let index = recurringDays.indexOf(day);
      if (index > -1) {
        recurringDays.splice(index, 1);
      }
    }

    if (e.target.name === WEEKLY) {
      setDaysOfWeek(recurringDays.toLocaleString());
    } else {
      setDaysOfMonth(recurringDays.toLocaleString());
    }
  };

  const daysOfTheWeek = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];

  const daysOfTheMonth = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21,
    22, 23, 24, 25, 26, 27, 28,
  ];

  return (
    <Fragment>
      <Button variant="contained" onClick={handleOpen}>
        Update
      </Button>
      <Modal
        open={modalIsOpen}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box className="updateStyle">
          <Typography
            align="center"
            id="modal-modal-title"
            variant="h6"
            component="h2"
            className="heading"
          >
            Please edit details to make an update
          </Typography>

          <Grid container spacing={3}>
            <Grid item xs={12}>
              <FormControl fullWidth>
                <label>Desk Location</label>
                <select disabled={true} className="select">
                  <option>
                    {book.desk.location === "SH"
                      ? "Sonnidom House"
                      : "Advantage Place"}
                  </option>
                </select>
              </FormControl>
            </Grid>

            <Grid item xs={12}>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <FormControl fullWidth>
                  <MobileDatePicker
                    className="select"
                    label="Start Date"
                    value={updateStartDate}
                    inputFormat="dd/MM/yyyy"
                    onChange={(date) => {
                      selectStartDateHandler(date);
                      checkDeskAvailability();
                    }}
                    renderInput={(params) => (
                      <TextField fullWidth {...params} />
                    )}
                  />
                </FormControl>
              </LocalizationProvider>
            </Grid>
            <Grid item xs={12}>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <FormControl fullWidth>
                  <MobileDatePicker
                    className="select"
                    label="End Date"
                    value={updateEndDate}
                    inputFormat="dd/MM/yyyy"
                    onChange={(date) => {
                      selectEndDateHandler(date);
                      checkDeskAvailability();
                    }}
                    renderInput={(params) => (
                      <TextField fullWidth {...params} />
                    )}
                  />
                </FormControl>
              </LocalizationProvider>
            </Grid>

            <Grid item xs={12}>
              {book.daysOfMonth && (
                <Fragment>
                  {daysOfTheMonth.map((day, index) => (
                    <FormControlLabel
                      key={index}
                      onChange={(e) => {
                        getRepeatData(e);
                        checkDeskAvailability();
                      }}
                      control={
                        <Checkbox
                          defaultChecked={bookedDaysOfMonth.includes(
                            day.toString()
                          )}
                          name={MONTHLY}
                          value={day}
                        />
                      }
                      label={day}
                    />
                  ))}
                </Fragment>
              )}

              {book.daysOfWeek && (
                <Fragment>
                  {daysOfTheWeek.map((day, index) => (
                    <FormControlLabel
                      key={index}
                      onChange={(e) => {
                        getRepeatData(e);
                        checkDeskAvailability();
                      }}
                      control={
                        <Checkbox
                          name={WEEKLY}
                          defaultChecked={bookedDaysOfWeek.includes(day)}
                          value={day}
                        />
                      }
                      label={day}
                    />
                  ))}
                </Fragment>
              )}
            </Grid>

            <Grid item xs={12}>
              <FormControl fullWidth>
                {desks.length > 0 && (
                  <AvailableDesks
                    book={book}
                    startDate={updateStartDate}
                    endDate={updateEndDate}
                    isAvailable={isAvailable}
                    daysOfMonth={daysOfMonth}
                    daysOfWeek={daysOfWeek}
                  />
                )}
              </FormControl>
            </Grid>
          </Grid>
        </Box>
      </Modal>
    </Fragment>
  );
}
