import { Typography } from "@mui/material";
import useStyles from "../assets/styles/style";
import "../assets/css/style.css";
import Navbar from "../component/Navbar";
import useFetch from "../shared/useFetch";
import BookedData from "./BookedData";
import { BASE_URL, API_VERSION } from "../shared/config";
import IdealTimmerComponent from "../shared/IdealTimmerComponent";

const UserBookings = () => {
  const classes = useStyles();
  const userData = JSON.parse(localStorage.getItem("loginData"));
  const userId = userData.userId;
  const { data, isPending, error } = useFetch(
    `${BASE_URL}/${API_VERSION}/bookings/${userId}`
  );

  return (
    <div>
      <Navbar />
      <div className={classes.centerElement}>
        <div className="bookingCard">
          <Typography className="title-sm">My Bookings</Typography>
          {error && <div>{error}</div>}
          {isPending && <div align="center">Loading....</div>}
          <div>
            {data && (
              <BookedData
                bookings={data}
              />
            )}
          </div>
        </div>
      </div>
      <IdealTimmerComponent />
    </div>
  );
};

export default UserBookings;
