import * as React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Slide from "@mui/material/Slide";
import { BASE_URL, API_VERSION } from "../shared/config";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const CancelBooking = ({ bookingId }) => {
  const userData = JSON.parse(localStorage.getItem("loginData"));
  const [open, setOpen] = React.useState(false);
  const modalOpenHandler = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const confirmCancelBookingHandler = () => {
    fetch(`${BASE_URL}/${API_VERSION}/bookings/${bookingId}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${userData.token}`,
      },
    })
      .then((res) => res.json())
      .then((res) => {})
      .catch((err) => {});
    setOpen(false);
    window.location.reload();
  };

  return (
    <span style={{ margin: "2px" }}>
      <Button variant="contained" color="warning" onClick={modalOpenHandler}>
        Cancel{" "}
      </Button>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle variant="h5">{"Confirm Cancel Booking?"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            {" "}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>No</Button>
          <Button onClick={confirmCancelBookingHandler} variant="contained">
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </span>
  );
};

export default CancelBooking;
