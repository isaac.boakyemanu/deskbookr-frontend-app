import * as fetch from "./fetch";
import * as func from "./function";

export { func, fetch };
