import { func } from ".";

export const get = async (url) => {
  const token = func.getStorage("loginData").token;
  const abortCont = new AbortController();

  return await fetch(url, {
    signal: abortCont.signal,
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
  })
    .then((res) => {
      if (res.status === 401) {
        localStorage.clear();
        window.location.href = "/";
      }
      if (!res.ok) {
        throw Error("Could not fetch the data for the resource");
      }
      return res.json();
    })
    .then((data) => {
      return data;
    })
    .catch((error) => {
      throw new Error(error);
    });
};
