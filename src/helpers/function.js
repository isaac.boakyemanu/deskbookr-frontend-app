/**
 * @param {string} key
 */
export const getStorage = (key) => {
  const value = JSON.parse(localStorage.getItem(key));
  return value || "";
};

/**
 * @param {Object} array1
 * @param {Object} array2
 */
export const getArrayWithData = (array1, array2) => {
  return array1.length >= 0 ? array1 : array2;
};

/**
 * @param {Object} array
 * @param {string} element
 */
export const removeElementFromObject = (array, element) => {
  return array.filter((item) => {
    return item !== element;
  });
};
