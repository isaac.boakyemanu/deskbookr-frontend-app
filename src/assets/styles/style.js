import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
 
  modal: {
    border: "1px solid black",
  },
  modalBody: {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    bgcolor: "background.paper",
    pt: 2,
    px: 4,
    pb: 3,
    width: 500,
    height: 500,
    borderRadius: 3,
  },
 
  userBookingButton: {
    marginRight: "10px",
  },
  centerElement: {
    margin: "20px auto",
    width: "65%",
    fontSize: "70px",
  },

  actionButtonContainer: {
    display: "flex",
    paddingLeft: "20%",
    paddingRight: "20%",
    justifyContent: "space-between",
    alignItems: "space-between",
  },
}));

export default useStyles;
