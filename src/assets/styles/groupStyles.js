export const cardStyles = {
    width: '1050px',
    position: 'relative',
    left: '50%',
    transform: 'translateX(-50%)',
    borderRadius: '8px',
};

export const cardHeaderStyles = {
    wrapper: {
        display: 'flex',
        // color: 'black',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingLeft: '20px',
        paddingRight: '20px',
        height: '70px',
        backgroundColor: '#f5f5f5',
        borderBottom: '1px solid rgba(0, 0, 0, 0.1)',
    },
    addUserButton: {
        fontSize: '1.05rem',
    },
}